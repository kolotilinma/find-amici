//
//  PetModel.swift
//  findAmici
//
//  Created by Михаил on 27.11.2020.
//

import Foundation

struct PetModel: Codable {
    
    var id: String
    let petName: String
    let ownerUid: String
    let ownerName: String
    var imageUrl: [String]
    let breed: String
    let age: String
    let sex: String
    let size: String
    let health: String
    let haveMicrochipImplant: Bool
    let haveRabiesVACCINATION: Bool
    let haveFVRCPVACCINATION: Bool
    let haveFeLVVACCINATION: Bool
    let petStory: String
    let petType: Int
    
    
}
