//
//  OutgoingMessage.swift
//  Messager
//
//  Created by Михаил on 15.11.2020.
//

import UIKit
import FirebaseFirestoreSwift
import Gallery

class OutgoingMessage {
    
    class func send(chatId: String, text: String?, photo: UIImage?, video: Video?, audio: String?, audioDuration: Float = 0.0, location: String?, memberIds: [String]) {
        let currentUser = User.currentUser!
        
        let message = LocalMessage()
        message.id = UUID().uuidString
        message.chatRoomId = chatId
        message.senderId = currentUser.id
        message.senderName = currentUser.username
        message.senderInitials = String(currentUser.username.first!)
        message.date = Date()
        message.status = Constant.kSent
        
        if text != nil {
            sendTextMessage(message: message, text: text!, memberIds: memberIds)
        }
        if photo != nil {
            sendPhotoMessage(message: message, photo: photo!, memberIds: memberIds)
        }
        if video != nil {
            sendVideoMessage(message: message, video: video!, memberIds: memberIds)
        }
        if location != nil {
            sendLocationMessage(message: message, memberIds: memberIds)
        }
        if audio != nil {
            sendAudioMessage(message: message, audioFileName: audio!, audioDuration: audioDuration, memberIds: memberIds)
        }
        
        //TODO: Sent push notification
        FirebaseRecentListener.shared.updateRecents(chatRoomId: chatId, lastMessage: message.message)
    }
    
    
    class func sendMessage(message: LocalMessage, memberIds: [String]) {
        RealmManager.shared.saveToRealm(message)
        for memberId in memberIds {
            FirebaseMessageListener.shared.addMessage(message, memberId: memberId)
        }
    }
    
    
    
}

func sendTextMessage(message: LocalMessage, text: String, memberIds: [String]) {
    message.message = text
    message.type = Constant.kText
    OutgoingMessage.sendMessage(message: message, memberIds: memberIds)
}

func sendPhotoMessage(message: LocalMessage, photo: UIImage, memberIds: [String]) {
    message.message = "Picture Message"
    message.type = Constant.kPhoto
    let fileName = Date().stringDate()
    let fileDirectory = "MediaMessages/Photo/" + "\(message.chatRoomId)" + "_\(fileName).jpg"
    FileStorage.saveFileLocally(fileData: photo.jpegData(compressionQuality: 0.6)! as NSData, fileName: fileName)
    
    FileStorage.uploadImage(photo, directory: fileDirectory) { (imageUrl) in
        if imageUrl != nil {
            message.pictureUrl = imageUrl ?? ""
            OutgoingMessage.sendMessage(message: message, memberIds: memberIds)
        }
    }
}


func sendVideoMessage(message: LocalMessage, video: Video, memberIds: [String])//, channel: Channel? = nil)
{
        
    message.message = "Video Message"
    message.type = Constant.kVideo
    
    let fileName = Date().stringDate()
    let thumbnailDirectory = "MediaMessages/Photo/" + "\(message.chatRoomId)/" + "_\(fileName)" + ".jpg"
    let videoDirectory = "MediaMessages/Video/" + "\(message.chatRoomId)/" + "_\(fileName)" + ".mov"
    let editor = VideoEditor()
    
    editor.process(video: video) { (precessedVideo, videoUrl) in
        if let tempPath = videoUrl {
            let thubnail = videoThumbnail(video: tempPath)
            FileStorage.saveFileLocally(fileData: thubnail.jpegData(compressionQuality: 0.7)! as NSData, fileName: fileName)
            FileStorage.uploadImage(thubnail, directory: thumbnailDirectory) { (imageLink) in
                if imageLink != nil {
                    let videoData = NSData(contentsOfFile: tempPath.path)
                    FileStorage.saveFileLocally(fileData: videoData!, fileName: fileName + ".mov")
                    FileStorage.uploadVideo(videoData!, directory: videoDirectory) { (videoLink) in
                        message.pictureUrl = imageLink ?? ""
                        message.videoUrl = videoLink ?? ""
                        
//                        if channel != nil {
//                            OutgoingMessage.sendChannelMessage(message: message, channel: channel!)
//                        } else {
                            OutgoingMessage.sendMessage(message: message, memberIds: memberIds)
//                        }
                    }
                }
            }
        }
    }
}

func sendLocationMessage(message: LocalMessage, memberIds: [String]) //, channel: Channel? = nil)
{
    let currentLocation = LocationManager.shared.currentLocation
    message.message = "Location message"
    message.type = Constant.kLocation
    message.latitude = currentLocation?.latitude ?? 0.0
    message.longitude = currentLocation?.longitude ?? 0.0
    
//    if channel != nil {
//        OutgoingMessage.sendChannelMessage(message: message, channel: channel!)
//    } else {
        OutgoingMessage.sendMessage(message: message, memberIds: memberIds)
//    }
}

func sendAudioMessage(message: LocalMessage, audioFileName: String, audioDuration: Float, memberIds: [String])//, channel: Channel? = nil) {
{
    message.message = "Audio message"
    message.type = Constant.kAudio
    let fileDirectory =  "MediaMessages/Audio/" + "\(message.chatRoomId)/" + "_\(audioFileName)" + ".m4a"
    FileStorage.uploadAudio(audioFileName, directory: fileDirectory) { (audioUrl) in
        if audioUrl != nil {
            message.audioUrl = audioUrl ?? ""
            message.audioDuration = Double(audioDuration)
//            if channel != nil {
//                OutgoingMessage.sendChannelMessage(message: message, channel: channel!)
//            } else {
                OutgoingMessage.sendMessage(message: message, memberIds: memberIds)
//            }
        }
    }
    
    
}
