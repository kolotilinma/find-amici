//
//  LocationMessage.swift
//  Messager
//
//  Created by Михаил on 17.11.2020.
//

import UIKit
import CoreLocation
import MessageKit

class LocationMessage: NSObject, LocationItem {
    
    var location: CLLocation
    var size: CGSize
    
    init(location: CLLocation) {
        self.location = location
        self.size = CGSize(width: 240, height: 240)
    }
}
