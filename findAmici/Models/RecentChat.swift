//
//  RecentChat.swift
//  Messager
//
//  Created by Михаил on 14.11.2020.
//

import Foundation
import FirebaseFirestoreSwift

struct RecentChat: Codable {
    var id = ""
    var chatRoomId = ""
    var senderId = ""
    var senderName = ""
    var receiverId = ""
    var receiverName = ""
    @ServerTimestamp var date = Date()
    var membersId = [""]
    var lastMessage = ""
    var unreadCounter = 0
    var avatarLink = ""
    
}
