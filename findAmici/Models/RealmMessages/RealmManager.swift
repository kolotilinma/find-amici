//
//  RealmManager.swift
//  Messager
//
//  Created by Михаил on 14.11.2020.
//

import Foundation
import RealmSwift

class RealmManager {
    
    static let shared = RealmManager()
    let realm = try! Realm()
    
    private init() {}
    
    func saveToRealm<T:Object>(_ object:T) {
        do {
            try realm.write {
                realm.add(object, update: .all)
            }
        } catch {
            print("Error saving realm Object: ", error.localizedDescription)
        }
    }
    
}
