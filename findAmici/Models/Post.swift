//
//  Post.swift
//  findAmici
//
//  Created by Михаил on 26.11.2020.
//

import Firebase
import FirebaseFirestoreSwift

struct Post: Codable {
    var caption: String
    var likes: Int
    let imageUrl: String
    @ServerTimestamp var timestamp = Date()
//    let timestamp: Timestamp
    let postId: String
    let ownerUid: String
    let ownerUsername: String
    let ownerImageUrl: String

    
//    init(postId: String, dictionary: [String: Any]) {
//        self.postId = postId
//        self.caption = dictionary["caption"] as? String ?? ""
//        self.likes = dictionary["likes"] as? Int ?? 0
//        self.imageUrl = dictionary["imageUrl"] as? String ?? ""
//        self.timestamp = dictionary["timestamp"] as? Timestamp ?? Timestamp(date: Date())
//        self.ownerUid = dictionary["ownerUid"] as? String ?? ""
//        self.ownerUsername = dictionary["ownerUsername"] as? String ?? ""
//        self.ownerImageUrl = dictionary["ownerImageUrl"] as? String ?? ""
//    }
}
