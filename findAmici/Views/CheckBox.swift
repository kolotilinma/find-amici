//
//  CheckBox.swift
//  findAmici
//
//  Created by Михаил on 28.11.2020.
//

import UIKit

class CheckBox: UIButton {
    //images
    let checkedImage = UIImage(named: "checkedImage")
    let unCheckedImage = UIImage(named: "unCheckedImage")
    
    //bool propety
    @IBInspectable var isChecked:Bool = false{
        didSet{
            self.updateImage()
        }
    }

    
    override func awakeFromNib() {
        self.addTarget(self, action: #selector(CheckBox.buttonClicked), for: .touchUpInside)
        self.updateImage()
    }
    
    
    func updateImage() {
        if isChecked == true{
            self.setImage(checkedImage, for: [])
        }else{
            self.setImage(unCheckedImage, for: [])
        }

    }

    @objc func buttonClicked(sender:UIButton) {
        if(sender == self){
            isChecked = !isChecked
        }
    }

}
