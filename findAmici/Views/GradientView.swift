//
//  GradientView.swift
//  findAmici
//
//  Created by Михаил on 11.11.2020.
//

import UIKit

class GradientView: UIView {

    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    @IBInspectable var leftColor: UIColor = #colorLiteral(red: 1, green: 0.7294117647, blue: 0.3254901961, alpha: 1) {
        didSet {
            setGradientColors()
        }
    }

    @IBInspectable var rightColor: UIColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0.4156862745, alpha: 1) {
        didSet {
            setGradientColors()
        }
    }
    
    lazy var gradientLayer: CAGradientLayer = {
        return self.layer as! CAGradientLayer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setGradientColors()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setGradientColors()
    }
    
    func setGradientColors() {
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [leftColor.cgColor, rightColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
    }
    
}
