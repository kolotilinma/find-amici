//
//  PetCellViewModel.swift
//  findAmici
//
//  Created by Михаил on 27.11.2020.
//

import UIKit


struct PetCellViewModel {
    
    let pet: PetModel
    
    var petProfileImageUrl: [URL?] {
        let petUrls = pet.imageUrl.map {
            return URL(string: $0)
        }
        return petUrls
    }
    var petName: String { return pet.petName }
    var dreed: String { return pet.breed }
    var age: String { return pet.age }
    var sex: String { return pet.sex }
    
    var sexImage: UIImage {
        if pet.sex == "Male" {
            return #imageLiteral(resourceName: "male")
        } else {
            return #imageLiteral(resourceName: "female")
        }
    }
    
    init(pet: PetModel) {
        self.pet = pet
    }
    
}
