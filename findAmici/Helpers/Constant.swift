//
//  Constant.swift
//  findAmici
//
//  Created by Михаил on 11.11.2020.
//

import Foundation

struct Constant {
    static let kAppCenterKey = "4f60af1f-54f2-4baf-a1d2-5d252cdb13fd"
    static let kFileReference = "gs://findamici-21a83.appspot.com"
    
    static let kCurrentUser = "CurrentUser"
    
    static let kStatus = "Status"
    static let kFirstRun = "firstRun"
    static let kChatRoomId = "chatRoomId"
    static let kSenderId = "senderId"
    
    //Message
    static let kNumberOfMessages = 15
    
    static let kSent = "Sent"
    static let kRead = "Read"
    static let kReadDate = "ReadDate"
    
    static let kText = "text"
    static let kPhoto = "photo"
    static let kVideo = "video"
    static let kAudio = "audio"
    static let kLocation = "location"
    
    static let kDate = "date"
    
    
}
