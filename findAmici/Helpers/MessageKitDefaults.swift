//
//  MessageKitDefaults.swift
//  Messager
//
//  Created by Михаил on 14.11.2020.
//

import UIKit
import MessageKit

struct MKSender: SenderType, Equatable {
    var senderId: String
    
    var displayName: String
    
}

enum MessageDefaults {
    //Bubble
    static let bubbleColorOutgoing = UIColor(named: "chatOutgoing") ?? UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
    static let bubbleColorIncoming = UIColor(named: "chatIncoming") ?? UIColor(red: 230/255, green: 229/255, blue: 234/255, alpha: 1)
}
