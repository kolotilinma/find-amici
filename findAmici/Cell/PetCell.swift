//
//  PetCell.swift
//  findAmici
//
//  Created by Михаил on 30.11.2020.
//

import UIKit

class PetCell: UICollectionViewCell {
    
    //MARK: - Properties
    var imageUrl: String? {
        didSet { configure() }
    }
    
    //MARK: - IBOutlet
    @IBOutlet weak var petImageView: UIImageView!
    
    //MARK: - View LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    private func configure() {
        let imageURL = URL(string: self.imageUrl ?? "")
        petImageView.sd_setImage(with: imageURL)
    }
}
