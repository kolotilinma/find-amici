//
//  NewPetsCell.swift
//  findAmici
//
//  Created by Михаил on 29.11.2020.
//

import UIKit

protocol NewPetsCellDelegate: class {
    func cell(wantsToDeleteCell cell: NewPetsCell)
}

class NewPetsCell: UICollectionViewCell {
    
    //MARK: - Properties
    weak var delegate: NewPetsCellDelegate?
    
    //MARK: - IBOutlet
    @IBOutlet weak var petImageView: UIImageView!
    
    
    //MARK: - View LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    @IBAction func deleteCellButtonTapped(_ sender: Any) {
        delegate?.cell(wantsToDeleteCell: self)
    }
    
}
