//
//  FindPetsCell.swift
//  findAmici
//
//  Created by Михаил on 27.11.2020.
//

import UIKit
import SDWebImage


class FindPetsCell: UITableViewCell {

    //MARK: - Properties
    var viewModel: PetCellViewModel? {
        didSet { configure() }
    }
    
    private let petImageView: UIImageView = {
        let iv = UIImageView()
//        iv.image = #imageLiteral(resourceName: "catImage")
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 5
        iv.clipsToBounds = true
        iv.backgroundColor = .lightGray
        return iv
    }()
    
    private let petSexImageView: UIImageView = {
        let iv = UIImageView()
//        iv.image = #imageLiteral(resourceName: "male")
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        return iv
    }()
    
    private let namePetLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    private let breedPetLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    private let agePetLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    private let locationPetLabel: UILabel = {
        let label = UILabel()
        label.text = "Naples, Italy (1 km)"
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .lightGray
        return label
    }()
    
    private lazy var likeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "pencil"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        button.tintColor = #colorLiteral(red: 0.9921568627, green: 0.5843137255, blue: 0.4823529412, alpha: 1)
        button.addTarget(self, action: #selector(didTapLikeButton), for: .touchUpInside)
        return button
    }()
    
    //MARK: - View LifeCycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(petImageView)
        petImageView.centerY(inView: self, leftAnchor: leftAnchor, paddingLeft: 5)
        petImageView.setDimensions(height: 100, width: 100)
        
        addSubview(petSexImageView)
        petSexImageView.anchor(top: self.topAnchor, left: petImageView.rightAnchor, paddingTop: 8, paddingLeft: 10)
        petSexImageView.setDimensions(height: 18, width: 18)
        
        addSubview(namePetLabel)
        namePetLabel.anchor(top: self.topAnchor, left: petSexImageView.rightAnchor, right: self.rightAnchor,
                            paddingTop: 8, paddingLeft: 8, paddingRight: 8)
        
        addSubview(likeButton)
        likeButton.centerY(inView: self)
        likeButton.anchor(right: self.rightAnchor, paddingRight: 5)
        likeButton.setDimensions(height: 40, width: 40)
        
        addSubview(breedPetLabel)
        breedPetLabel.anchor(top: namePetLabel.bottomAnchor, left: petImageView.rightAnchor, right: likeButton.leftAnchor,
                             paddingTop: 8, paddingLeft: 10, paddingRight: 10)
        
        addSubview(agePetLabel)
        agePetLabel.anchor(top: breedPetLabel.bottomAnchor, left: petImageView.rightAnchor, right: likeButton.leftAnchor,
                           paddingTop: 2, paddingLeft: 10, paddingRight: 10)
        
        let locationImageView = UIImageView(image: UIImage(systemName: "location"))
        locationImageView.contentMode = .scaleAspectFill
        locationImageView.tintColor = .lightGray
//        locationImageView.clipsToBounds = true
        addSubview(locationImageView)
        locationImageView.anchor(left: petImageView.rightAnchor, bottom: self.bottomAnchor,
                                 paddingLeft: 10, paddingBottom: 8, width: 11, height: 16)
        
        addSubview(locationPetLabel)
        locationPetLabel.anchor(left: locationImageView.rightAnchor, bottom: self.bottomAnchor, right: self.rightAnchor,
                                paddingLeft: 8, paddingBottom: 8, paddingRight: 10)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func didTapLikeButton() {
        print(#function)
    }
    
    private func configure() {
        guard let viewModel = viewModel else { return }
        guard let imageUrl = viewModel.petProfileImageUrl.first else { return }
        petImageView.sd_setImage(with: imageUrl)
        namePetLabel.text = viewModel.petName
        breedPetLabel.text = viewModel.dreed
        agePetLabel.text = viewModel.age
        petSexImageView.image = viewModel.sexImage
    }
    
}
