//
//  Date.swift
//  Messager
//
//  Created by Михаил on 14.11.2020.
//

import UIKit

extension Date {
    func longDate() -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd MM yyyy"
        return dateFormater.string(from: self)
    }
    
    func time() -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "HH:mm"
        return dateFormater.string(from: self)
    }
    
    func stringDate() -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "ddMMyyyyHHmmssSSS"
        return dateFormater.string(from: self)
    }
    
    func interval(ofComponent comp: Calendar.Component, from date: Date) -> Float {
        let currentCalendar = Calendar.current
        guard  let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard  let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        return Float(start - end)
    }
}
