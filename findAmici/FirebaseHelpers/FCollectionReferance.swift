//
//  FCollectionReferance.swift
//  findAmici
//
//  Created by Михаил on 11.11.2020.
//

import Foundation
import FirebaseFirestore

enum FCollectionReferance: String {
    case User
    case Recent
    case Messages
    case Typing
    case Channel
    case Posts
    case Pets
}

func FirebaseReference(_ collectionReference: FCollectionReferance) -> CollectionReference {
    return Firestore.firestore().collection(collectionReference.rawValue)
}
