//
//  PostService.swift
//  findAmici
//
//  Created by Михаил on 27.11.2020.
//

import UIKit
import Firebase

typealias FirestoreCompletion = (Error?) -> Void

struct PostService {
    static func uploadPost(caption: String, postImage: UIImage, user: User, completion: @escaping(FirestoreCompletion)) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let fileName = NSUUID().uuidString
        let fileDirectory = "PostsMedia" + "_\(fileName).jpg"
        
        FileStorage.uploadImage(postImage, directory: fileDirectory) { (imageUrl) in
            guard let imageUrl = imageUrl else { return }
            let post = Post(caption: caption, likes: 0, imageUrl: imageUrl, postId: fileName, ownerUid: uid, ownerUsername: user.username, ownerImageUrl: user.avatarLink)
            
            do {
                let _ = try FirebaseReference(.Posts).document(post.postId).setData(from: post, completion: completion)
            } catch  {
                print("DEBUG: Error saving post: ", error.localizedDescription)
            }
        }
    }
    
    static func fetchPosts(completion: @escaping([Post]) -> Void) {
        FirebaseReference(.Posts).order(by: "timestamp", descending: true).getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents else { return }
            var posts = documents.compactMap{ (queryDocumentSnapshot) -> Post? in
                return try? queryDocumentSnapshot.data(as: Post.self)
            }
            posts.sort { (post1, post2) -> Bool in
                return post1.timestamp! > post2.timestamp!
            }
            completion(posts)
        }
    }
    
    static func fetchPosts(forUser uid: String, completion: @escaping([Post]) -> Void) {
        FirebaseReference(.Posts).whereField("ounerUid", isEqualTo: uid).getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents else { return }

            var posts = documents.compactMap{ (queryDocumentSnapshot) -> Post? in
                return try? queryDocumentSnapshot.data(as: Post.self)
            }
            posts.sort { (post1, post2) -> Bool in
                return post1.timestamp! > post2.timestamp!
            }
            completion(posts)
        }
    }
    
}
