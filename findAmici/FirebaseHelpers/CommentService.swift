//
//  CommentService.swift
//  findAmici
//
//  Created by Михаил on 03.12.2020.
//

import Firebase

struct CommentService {
    
    static func uploadComment(comment: String, postId: String, user: User, completion: @escaping(FirestoreCompletion)) {
        
        let data: [String: Any] = ["uid": user.id,
                                   "comment": comment,
                                   "timestamp": Timestamp(date: Date()),
                                   "username": user.username,
                                   "profileImageUrl": user.avatarLink]
        FirebaseReference(.Posts).document(postId).collection("comments").addDocument(data: data, completion: completion)
        
    }
    
    static func fetchComments(postId: String, completion: @escaping([Comment]) -> Void) {
        var comments = [Comment]()
        let query = FirebaseReference(.Posts).document(postId).collection("comments").order(by: "timestamp", descending: true)
        query.addSnapshotListener { (snapshot, error) in
            snapshot?.documentChanges.forEach({ (change) in
                if change.type == .added {
                    let data = change.document.data()
                    let comment = Comment(dictionary: data)
                    comments.append(comment)
                }
            })
            completion(comments)
        }
        
    }
    
}
