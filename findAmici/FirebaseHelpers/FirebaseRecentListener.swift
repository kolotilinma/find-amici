//
//  FirebaseRecentListener.swift
//  Messager
//
//  Created by Михаил on 14.11.2020.
//

import Foundation
import Firebase

class FirebaseRecentListener {
    
    static let shared = FirebaseRecentListener()
    
    private init() {}
    
    func downloadRecentChatFromFirebase(completion: @escaping (_ allRecents: [RecentChat]) -> Void) {
        FirebaseReference(.Recent).whereField(Constant.kSenderId, isEqualTo: User.currentId).addSnapshotListener { (querySnapshot, error) in
            var recentChats: [RecentChat] = []
            guard let documents = querySnapshot?.documents else { return }
            let allRecents = documents.compactMap { (queryDocumentSnapshot) -> RecentChat? in
                return try? queryDocumentSnapshot.data(as: RecentChat.self)
            }
            for recent in allRecents {
                if recent.lastMessage != "" {
                    recentChats.append(recent)
                }
            }
            recentChats.sort(by: { $0.date! > $1.date!})
            completion(recentChats)
        }
    }
    
    func resetRecentCounter(chatRoomId: String) {
        FirebaseReference(.Recent).whereField(Constant.kChatRoomId, isEqualTo: chatRoomId).whereField(Constant.kSenderId, isEqualTo: User.currentId).getDocuments { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                print("Error: No documents")
                return
            }
            let allRecents = documents.compactMap { (queryDocumentSnapshot) -> RecentChat? in
                return try? queryDocumentSnapshot.data(as: RecentChat.self)
            }
            if allRecents.count > 0 {
                self.clearUnreadCounter(allRecents.first!)
            }
        }
    }
    
    func updateRecents(chatRoomId: String, lastMessage: String) {
        FirebaseReference(.Recent).whereField(Constant.kChatRoomId, isEqualTo: chatRoomId).getDocuments { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                print("Error: no document for recent update")
                return
            }
            let allRecents = documents.compactMap { (queryDocumentSnapshot) -> RecentChat? in
                return try? queryDocumentSnapshot.data(as: RecentChat.self)
            }
            for recentChat in allRecents {
                self.updateRecentItemWithNewMessage(recent: recentChat, lastMessage: lastMessage)
            }
        }
    }
    
    private func updateRecentItemWithNewMessage(recent: RecentChat, lastMessage: String) {
        var tempRecent = recent
        if tempRecent.senderId != User.currentId {
            tempRecent.unreadCounter += 1
        }
        tempRecent.lastMessage = lastMessage
        tempRecent.date = Date()
        self.saveRecent(tempRecent)
    }
    
    func clearUnreadCounter(_ recent: RecentChat) {
        var newRecent = recent
        newRecent.unreadCounter = 0
        self.saveRecent(newRecent)
    }
    
    func saveRecent(_ recent: RecentChat) {
        do {
            try FirebaseReference(.Recent).document(recent.id).setData(from: recent)
        } catch let error {
            print("Error: Error saving recent chat! ",error.localizedDescription)
        }
    }
    
    func deleteRecent(_ recent: RecentChat) {
        FirebaseReference(.Recent).document(recent.id).delete()
    }
    
}
