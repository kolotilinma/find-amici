//
//  PetsService.swift
//  findAmici
//
//  Created by Михаил on 27.11.2020.
//

import Foundation
import Firebase

struct PetsService {

    static func uploadNewPet(pet: PetModel, petsImages: [UIImage], completion: @escaping(FirestoreCompletion)) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        var pet = pet
        petsImages.forEach { (image) in
            let fileName = NSUUID().uuidString
            let fileDirectory = "PetsMedia/" + "\(uid)/" + "_\(fileName).jpg"
            
            FileStorage.uploadImage(image, directory: fileDirectory) { (imageUrl) in
                guard let imageUrl = imageUrl else { return }
                pet.imageUrl.append(imageUrl)
                
                do {
                    let _ = try FirebaseReference(.Pets).document(pet.id).setData(from: pet, completion: completion)
                } catch  {
                    print("DEBUG: Error saving post: ", error.localizedDescription)
                }
            }
        }
    }
    
    static func fetchPets(completion: @escaping([PetModel]) -> Void) {
        FirebaseReference(.Pets).getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents else { return }
            let pets = documents.compactMap{ (queryDocumentSnapshot) -> PetModel? in
                return try? queryDocumentSnapshot.data(as: PetModel.self)
            }
            completion(pets)
        }
    }
    
    
}
