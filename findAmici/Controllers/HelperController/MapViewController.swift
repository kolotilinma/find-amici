//
//  MapViewController.swift
//  Messager
//
//  Created by Михаил on 18.11.2020.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {

    //MARK: - Vars
    var location: CLLocation?
    var mapView: MKMapView!

    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

//        configureTitle()
        configureMapView()
        configureLeftBarButton()
    }
    
    //MARK: - Configurations
    private func configureMapView() {
        mapView = MKMapView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        mapView.showsUserLocation = true
        if location != nil {
            mapView.setCenter(location!.coordinate, animated: false)
            let annotation = MapAnnotation(title: nil, coordinate: location!.coordinate)
            mapView.addAnnotation(annotation)
        }
        view.addSubview(mapView)
    }
    
    private func configureLeftBarButton() {
        let backImage = UIImage(systemName: "chevron.left")
        let item = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self.backButtonPressed))
        self.navigationItem.leftBarButtonItem = item
    }
    
    private func configureTitle() {
        self.title = "Map View"
    }

    //MARK: - Actions
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }

}
