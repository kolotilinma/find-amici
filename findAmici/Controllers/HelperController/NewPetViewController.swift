//
//  NewPetViewController.swift
//  findAmici
//
//  Created by Михаил on 27.11.2020.
//

import UIKit
import Gallery
import IQKeyboardManagerSwift

class NewPetViewController: UIViewController {
    //MARK: - Vars
    var maxImageLimit = 10
    var ageList = ["1 month", "2 month", "3 month", "4 month", "5 month", "6 month", "7 month", "8 month", "9 month", "10 month", "11 month", "1 year", "2 years", "3 years", "4 years", "5 years", "6 years", "7 years", "8 years", "9 years", "10 years"]
    var sexList = ["Male", "Famale"]
    var sizeList = ["mini", "small", "medium", "large", "huge"]
    var healthList = ["Good", "Bad"]
    
    //MARK: - Properties
    var petsImages: [UIImage] = []
    var gallery: GalleryController!
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var petTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var petsNameTextField: UITextField!
    @IBOutlet weak var petsBreedTextField: UITextField!
    @IBOutlet weak var petsAgeTextField: PickerTextField!
    @IBOutlet weak var petSexTextField: PickerTextField!
    @IBOutlet weak var petSizeTextField: PickerTextField!
    @IBOutlet weak var petHealthTextField: PickerTextField!
    @IBOutlet weak var petChipImplantButtonOutlet: CheckBox!
    @IBOutlet weak var petRabiesVaccinButtonOutlet: CheckBox!
    @IBOutlet weak var petFVRCPVaccinButtonOutlet: CheckBox!
    @IBOutlet weak var petFeLVVaccinButtonOutlet: CheckBox!
    @IBOutlet weak var petStoryTextView: UITextView!
    
    @IBOutlet weak var addAnimalButtonOutlet: UIButton!
    
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIQKeyboardManager()
        setupTextFields()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBActions
    @IBAction func cancelButtonDidTaped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addNewPhotoTapped(_ sender: Any) {
        showImageGallery()
    }
    
    @IBAction func addNewAnimalTapped(_ sender: Any) {
        guard let petName = petsNameTextField.text else { return }
        guard let petBreed = petsBreedTextField.text else { return }
        guard let petSex = petSexTextField.text else { return }
        guard let petSize = petSizeTextField.text else { return }
        guard let petHealth = petHealthTextField.text else { return }
        guard let petAge = petsAgeTextField.text else { return }
        
        
        let pet = PetModel(id: UUID().uuidString,
                           petName: petName,
                           ownerUid: User.currentId,
                           ownerName: User.currentUser!.username,
                           imageUrl: [],
                           breed: petBreed,
                           age: petAge,
                           sex: petSex,
                           size: petSize,
                           health: petHealth,
                           haveMicrochipImplant: petChipImplantButtonOutlet.isChecked,
                           haveRabiesVACCINATION: petRabiesVaccinButtonOutlet.isChecked,
                           haveFVRCPVACCINATION: petFVRCPVaccinButtonOutlet.isChecked,
                           haveFeLVVACCINATION: petFeLVVaccinButtonOutlet.isChecked,
                           petStory: petStoryTextView.text,
                           petType: petTypeSegmentedControl.selectedSegmentIndex)
        
        PetsService.uploadNewPet(pet: pet, petsImages: petsImages) { (error) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: - Gallery
    private func showImageGallery() {
        self.gallery = GalleryController()
        self.gallery.delegate = self
        
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = maxImageLimit
        Config.initialTab = .imageTab
        
        present(gallery, animated: true, completion: nil)
    }
    
    //MARK: - Helpers
    private func setupTextFields() {
        petsAgeTextField.dataSource = self
        petSexTextField.dataSource = self
        petSizeTextField.dataSource = self
        petHealthTextField.dataSource = self
    }
    
    private func setupIQKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        let keyboardHideImage = UIImage(systemName: "keyboard.chevron.compact.down")!
            .withTintColor(.darkGray, renderingMode: .alwaysTemplate)
        IQKeyboardManager.shared.toolbarDoneBarButtonItemImage = keyboardHideImage
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldPlayInputClicks = false
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 40
        IQKeyboardManager.shared.toolbarTintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    }

}

//MARK: - UICollectionViewDataSource
extension NewPetViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return petsImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewPetCell", for: indexPath) as! NewPetsCell
        
        cell.delegate = self
        cell.petImageView.image = petsImages[indexPath.row]
        return cell
    }
    
    
}

//MARK: - UICollectionViewDelegate
extension NewPetViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
}

//MARK: - UICollectionViewFlowLayout
extension NewPetViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 108, height: 108)
    }
}

//MARK: - NewPetsCellDelegate
extension NewPetViewController: NewPetsCellDelegate {
    func cell(wantsToDeleteCell cell: NewPetsCell) {
        guard let index = collectionView.indexPath(for: cell) else { return }
        petsImages.remove(at: index.row)
        maxImageLimit += 1
        collectionView.reloadData()
    }
}

//MARK: - GalleryControllerDelegate
extension NewPetViewController: GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        if images.count > 0 {
            images.forEach { (petImage) in
                petImage.resolve { (petUIImage) in
                    guard let image = petUIImage else { return }
                    self.petsImages.append(image)
                    self.maxImageLimit -= 1
                    self.collectionView.reloadData()
                }
                
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

//MARK: - PickerTextFieldDataSource
extension NewPetViewController: PickerTextFieldDataSource {
    func numberOfRows(in pickerTextField: PickerTextField) -> Int {
        if pickerTextField == petSexTextField {
            return sexList.count
        } else if pickerTextField == petSizeTextField {
            return sizeList.count
        } else if pickerTextField == petsAgeTextField {
            return ageList.count
        } else if pickerTextField == petHealthTextField {
            return healthList.count
        } else {
            return 0
        }
    }
    
    func pickerTextField(_ pickerTextField: PickerTextField, titleForRow row: Int) -> String? {
        if pickerTextField == petSexTextField {
            return sexList[row]
        } else if pickerTextField == petSizeTextField {
            return sizeList[row]
        } else if pickerTextField == petsAgeTextField {
            return ageList[row]
        } else if pickerTextField == petHealthTextField {
            return healthList[row]
        } else {
            return nil
        }
    }
}
