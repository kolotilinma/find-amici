//
//  PetViewController.swift
//  findAmici
//
//  Created by Михаил on 30.11.2020.
//

import UIKit
import SKPhotoBrowser
import ProgressHUD

class PetViewController: UIViewController {
 
    //MARK: - Properties
    var pet: PetModel! {
        didSet {  }
    }
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    @IBOutlet weak var petNameLabel: UILabel!
    @IBOutlet weak var petOwnerNameButtonOutlet: UIButton!
    @IBOutlet weak var petAgeLabel: UILabel!
    @IBOutlet weak var petSexLabel: UILabel!
    @IBOutlet weak var petHealthLabel: UILabel!
    @IBOutlet weak var petBreedLabel: UILabel!
    @IBOutlet weak var petSizeLabel: UILabel!
    @IBOutlet weak var petChipLabel: UILabel!
    @IBOutlet weak var petVaccinLabel: UILabel!
    @IBOutlet weak var petStoryTextView: UITextView!
    
    
    
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: - Helpers
    
    private func configureUI() {
        guard let pet = pet else { return }
        pageControl.numberOfPages = pet.imageUrl.count
        petNameLabel.text = pet.petName
        petOwnerNameButtonOutlet.setTitle(pet.ownerName, for: .normal)
        petAgeLabel.text = pet.age
        petSexLabel.text = pet.sex
        petHealthLabel.text = pet.health
        petBreedLabel.text = pet.breed
        petSizeLabel.text = pet.size
        petChipLabel.text = pet.haveMicrochipImplant ? "Yes" : "No"
        petStoryTextView.text = pet.petStory
    }
    
    //MARK: - Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showShelderProfile(_ sender: Any) {
        ProgressHUD.show()
        FirebaseUserListener.shared.dowloadUsersFromFirebase(withIds: [pet.ownerUid]) { (users) in
            let profileView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "profileView") as! ProfileTableViewController
            profileView.user = users.first
            ProgressHUD.dismiss()
            self.navigationController?.pushViewController(profileView, animated: true)
        }
    }
    
}

//MARK: - UICollectionViewDataSource
extension PetViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pet.imageUrl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PetCell", for: indexPath) as! PetCell
        
        cell.imageUrl = pet.imageUrl[indexPath.row]
        return cell
    }
    
    
}

//MARK: - UICollectionViewDelegate
extension PetViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
        let images = pet.imageUrl.map { SKPhoto.photoWithImageURL($0) }
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(indexPath.row)
        present(browser, animated: true, completion: nil)

    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }
    
}

//MARK: - UICollectionViewFlowLayout
extension PetViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 296)
    }
    
}
