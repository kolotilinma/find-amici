//
//  MainTabController.swift
//  findAmici
//
//  Created by Михаил on 26.11.2020.
//

import UIKit
import Firebase

class MainTabController: UITabBarController {

    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewControllers()

    }
    
    
    //MARK: - Helpers
    private func configureViewControllers() {
//        self.delegate = self
        
        let findPetsVC = FindPetsTableViewController()
        let findPetsNavVC = templateNavigationController(unselectedImage: #imageLiteral(resourceName: "search icon"), selectedImage: #imageLiteral(resourceName: "search icon").withRenderingMode(.alwaysTemplate), findPetsVC)
        
        let mapNavContr = templateNavigationController(unselectedImage: UIImage(systemName: "map")!,
                                                       selectedImage: UIImage(systemName: "map.fill")!,
                                                       MapViewController())
        
        
        let feedVC = FeedViewController(collectionViewLayout: UICollectionViewFlowLayout())
        let feedNavVC = templateNavigationController(unselectedImage: UIImage(systemName: "house")!,
                                                     selectedImage: UIImage(systemName: "house.fill")!,
                                                     feedVC)
        
        let chatsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ChatsVC") as! ChatsTableViewController
        let nacChatsVC = templateNavigationController(unselectedImage: UIImage(systemName: "message")!, selectedImage: UIImage(systemName: "message.fill")!, chatsVC)
        
        let profileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "SettingsVC") as! SettingsTableViewController
        let profileNavVC = templateNavigationController(unselectedImage: UIImage(systemName: "person")!,
                                                        selectedImage: UIImage(systemName: "person.fill")!,
                                                        profileVC)

        viewControllers = [findPetsNavVC, mapNavContr, feedNavVC, nacChatsVC, profileNavVC]
        tabBar.tintColor = #colorLiteral(red: 0.9921568627, green: 0.5843137255, blue: 0.4823529412, alpha: 1)
    }

    private func templateNavigationController(unselectedImage: UIImage, selectedImage: UIImage, _ rootController: UIViewController) -> UINavigationController {
        let navVC = UINavigationController(rootViewController: rootController)
        navVC.tabBarItem.image = unselectedImage
        navVC.tabBarItem.selectedImage = selectedImage
        navVC.navigationBar.tintColor = .black
        return navVC
    }
    
}
