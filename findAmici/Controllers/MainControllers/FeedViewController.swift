//
//  FeedViewController.swift
//  findAmici
//
//  Created by Михаил on 26.11.2020.
//

import UIKit
import Firebase
import YPImagePicker

private let reuseIdentifier = "Cell"

class FeedViewController: UICollectionViewController {

    //MARK: - Properties
    private var posts = [Post]()
    var post: Post?
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fetchPosts()
    }
    
    //MARK: - Actions
    @objc func handleNewPost() {
        
        var config = YPImagePickerConfiguration()
        config.library.mediaType = .photo
        config.shouldSaveNewPicturesToAlbum = false
        config.startOnScreen = .library
        config.screens = [.library]
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.library.maxNumberOfItems = 1
        
        let picker = YPImagePicker(configuration: config)
        picker.modalPresentationStyle = .fullScreen
        self.present(picker, animated: true, completion: nil)
        
        didFinishPicikingMedia(picker)
    }
    
    @objc func handleRefresh() {
        posts.removeAll()
        self.collectionView.reloadData()
        fetchPosts()
    }
    
    //MARK: - API
    private func fetchPosts() {
        guard post == nil else { return }
        PostService.fetchPosts { (posts) in
            self.posts = posts
            self.collectionView.refreshControl?.endRefreshing()
            self.collectionView.reloadData()
        }
    }
    
    //MARK: - Helpers
    private func configureUI() {
        collectionView.backgroundColor = .white
        collectionView.register(FeedCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        if post == nil {
            let image = UIImage(systemName: "square.and.pencil")
            image?.withTintColor(#colorLiteral(red: 0.9921568627, green: 0.5843137255, blue: 0.4823529412, alpha: 1))
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleNewPost))
            
            let refresher = UIRefreshControl()
            refresher.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
            collectionView.refreshControl = refresher
        }
        
    }
    
    func didFinishPicikingMedia(_ picker: YPImagePicker) {
        picker.didFinishPicking { (items, _) in
            picker.dismiss(animated: false) {
                guard let selectedImage = items.singlePhoto?.image else { return }
                
                let controller = UploadPostViewController()
                controller.delegate = self
                controller.currentUser = User.currentUser
                controller.selectedImage = selectedImage
                let navController = UINavigationController(rootViewController: controller)
                navController.modalPresentationStyle = .fullScreen
                self.present(navController, animated: false, completion: nil)
                
            }
        }
    }

}

//MARK: - UICollectionViewDataSource
extension FeedViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return post == nil ? posts.count : 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FeedCell
        cell.delegate = self
        if let post = post {
            cell.viewModel = PostViewModel(post: post)
        } else {
            cell.viewModel = PostViewModel(post: posts[indexPath.row])
        }
        return cell
    }
}

//MARK: - UICollectionViewFlowLayout
extension FeedViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width
        var height = width + 8 + 40 + 8
        height += 50
        height += 60
        
        return CGSize(width: width, height: height)
    }
}

//MARK: - FeedCellDelegate
extension FeedViewController: FeedCellDelegate {
    func cell(_ cell: FeedCell, wantsToShowCommentsFor post: Post) {
        let controller = CommentsController(post: post)
        
        navigationController?.pushViewController(controller, animated: true)
    }
}


//MARK: - UploadPostViewControllerDelegate
extension FeedViewController: UploadPostViewControllerDelegate {
    func controllerDidFinishUploadPost(_ controller: UploadPostViewController) {
//        tabBarController?.selectedIndex = 0
        controller.dismiss(animated: true, completion: nil)
        handleRefresh()
    }
}
