//
//  FindPetsTableViewController.swift
//  findAmici
//
//  Created by Михаил on 27.11.2020.
//

import UIKit


private let reuseIdentifier = "FindPetsCell"

class FindPetsTableViewController: UITableViewController {

    //MARK: - Properties
    var pets = [PetModel]() {
        didSet { tableView.reloadData() }
    }
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        setupUI()
        fetchPosts()
    }

    //MARK: - Actions
    @objc func handleAddNewPet() {
        let newPetVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "AddNewPetVC")
        newPetVC.modalPresentationStyle = .fullScreen
        self.present(newPetVC, animated: true, completion: nil)
    }
    
    //MARK: - API
    private func fetchPosts() {
        PetsService.fetchPets { (allPets) in
            self.pets = allPets
        }
    }
    
    //MARK: - Helpers
    private func configureTableView() {
        tableView.register(FindPetsCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.rowHeight = 110
        tableView.tableFooterView = UIView()
    }

    private func setupUI() {
        let image = UIImage(systemName: "square.and.pencil")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleAddNewPet))
    }

}

// MARK: - Table view data source
extension FindPetsTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pets.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! FindPetsCell
        
        cell.viewModel = PetCellViewModel(pet: pets[indexPath.row])
        return cell
    }
}

// MARK: - Table view
extension FindPetsTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "petVC") as! PetViewController
        controller.pet = pets[indexPath.row]
        navigationController?.pushViewController(controller, animated: true)
        
    }
}
